import {Container} from 'react-bootstrap';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';



export default function Home() {

	const data = {
		title : "Arn Thrift Shop",
		content : "The Thrift Shop in Ormoc",
		destination: "/products",
		label: "Order Now!"
	}
	return (
		<>
			<Container>
              	<Banner data={data}/>
              	<Highlights/>
            </Container>
		</>
	)
}
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	// The "useParams" hook allows us to retrieve the courseId passed via the URL
	const { productId } = useParams();

	// To be able to obtain the user ID so that we can enroll a user
	const { user } = useContext(UserContext);

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(0);

	useEffect(() => {
		console.log(productId);

		// Fetch request that will retrieve the details of the course from the database to be displayed in the "CourseView" page
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	// Function to order a product

	const order = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: "Order Added!",
					icon: "success",
					text: "You have successfully ordered this item!"
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}


const handleDecrement = () => {
		if(quantity > 0){
			setQuantity(quantity - 1)
		}
	}

const handleIncrement = () => {
		if (user.id !== null) {
		 	setQuantity(quantity + 1)
		}
	}



	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center mt-3">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					        <Card.Subtitle>Quantity</Card.Subtitle>
					        <div className="input-group justify-content-center mt-2 mb-3">
					        	<Button type="button" onClick={() => handleDecrement(productId)} className="input-group-text">-</Button>
					        	<div className="input-group-text text-center">{quantity}</div>
					        	<Button type="button" onClick={() => handleIncrement(productId)} className="input-group-text">+</Button>
					        </div>

					        {user.id !== null
					        	?
					        	<Button variant="primary" onClick={() => order(productId)}>Checkout</Button>
					        	:
					        	<Button as={Link} to="/login" variant="danger">Log in to Order</Button>
					        }

					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>
				       		<h2>The Best Thrift shop in Ormoc</h2>
				       </Card.Title>
				       <Card.Text>
				         Located in Ormoc City, Leyte
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>
				       		<h2>Affordable and Reliable.</h2>
				       </Card.Title>
				       <Card.Text>
				         We are on sale 50% off limited stocks 
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>
				       		<h2>All Branded made from US</h2>
				       </Card.Title>
				       <Card.Text>
				         We guarantee that our products maintain its quality.
				       </Card.Text>
				     </Card.Body>
				   </Card>
			</Col>
		</Row>
	)
}